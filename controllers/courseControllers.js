//Dependencies
const Course = require("../models/Course");
const bcrypt = require("bcryptjs");

const auth = require("../auth");

module.exports.registerCourse = (req, res) => {
    console.log(req.body);

    req.body;

    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    });
    newCourse
        .save()
        .then((course) => res.send(course))
        .catch((err) => res.send(err));
};

module.exports.getAllCourse = (req, res) => {
    Course.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

module.exports.getSingleCourseController = (req, res) => {
    Course.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//UPDATING A COURSE
module.exports.updateCourse = (req, res) => {
    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    };

    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((upCourse) => res.send(upCourse))
        .catch((error) => res.send(error));
};

//ARCHIVE A COURSE
module.exports.archiveCourse = (req, res) => {
    let archive = {
        isActive: false,
    };

    Course.findByIdAndUpdate(req.params.id, archive, { new: true })
        .then((upCourse) => res.send(upCourse))
        .catch((error) => res.send(error));
};

//ACTIVATE A COURSE
module.exports.activateCourse = (req, res) => {
    let activate = {
        isActive: true,
    };

    Course.findByIdAndUpdate(req.params.id, activate, { new: true })
        .then((upCourse) => res.send(upCourse))
        .catch((error) => res.send(error));
};

//Retrieve All Active COURSES
module.exports.retrieveActiveCourses = (req, res) => {
    Course.find({ isActive: true })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};