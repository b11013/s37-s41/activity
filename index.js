const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//server
const app = express();
const port = 4200;

//db connection
mongoose.connect(
    "mongodb+srv://admin:admin@capstone2.8kmft.mongodb.net/course-booking-182?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Database connection Failed"));
db.once("open", () => console.log("Database connection is NICE"));

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//group routing
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const courseRoutes = require("./routes/courseRoutes");
app.use("/courses", courseRoutes);

//port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));