App:
Booking System API

Desc:
Allows users to enroll to a course.
Allows Admin to perform CRUD operations on courses.
Allows regular users to register.

// USER Model:
// all value in the fields should be required
firstName: String,
lastName: String,
email: String,
password: String,
mobileNo: String,
isAdmin: boolean,
    default: false
enrollments: [{
    courseID: String,
    status: String,
        default:enrolled
    dateEnrolled: date
}]

//COURSE Model:
    name: string,
    description: string,
    price: number,
    isActive: boolean,
        default: true
    createdOn: date
        enrollees: [{
        userID: String,
        status: String,
        dateEnrolled: date
        }]

