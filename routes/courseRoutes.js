const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

//verify - only used when user is needed to be logged in to use the feature
//verifyAdmin - to check if user is logged in, if feature is only for admin
const { verify } = auth;
const { verifyAdmin } = auth;

//add course
router.post("/", verify, verifyAdmin, courseControllers.registerCourse);

//get all course
router.get("/", courseControllers.getAllCourse);

router.get("/getSingleCourse/:id", courseControllers.getSingleCourseController);

//updating a course via ID
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

//Archive a course
router.put(
    "/archive/:id",
    verify,
    verifyAdmin,
    courseControllers.archiveCourse
);

//Activate a course
router.put(
    "/activate/:id",
    verify,
    verifyAdmin,
    courseControllers.activateCourse
);

//Get all Active Course
router.get(
    "/getActiveCourses",
    verify,
    verifyAdmin,
    courseControllers.retrieveActiveCourses
);

module.exports = router;