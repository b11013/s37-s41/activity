//DEPENDENCIES
const express = require("express");
const router = express.Router();

//imported module
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

//object destructuring from auth module
const { verify } = auth;
const { verifyAdmin } = auth;

//ROUTES

//reg user
router.post("/", userControllers.registerUser);

//get all user
router.get("/", userControllers.getAllUsers);

//login user
router.post("/login", userControllers.loginUser);

//get user details
router.get("/getUserDetails", verify, userControllers.getUserDetails);

//check email dup
router.post("/checkEmailExists", userControllers.checkEmailControllers);

//updating user detail
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//update admin by an admin
router.put(
    "/updateAdmin/:id",
    verify,
    verifyAdmin,
    userControllers.updateAdmin
);

module.exports = router;